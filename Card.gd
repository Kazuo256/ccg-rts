extends Node3D

const HOVER_LIFT_HEIGHT := 0.5
const HOVER_SMOOTH_FACTOR := 10

@onready
var hovered := false

func _process(delta):
	if hovered:
		position.y += (HOVER_LIFT_HEIGHT - position.y) * HOVER_SMOOTH_FACTOR * delta
	else:
		position.y += (0 - position.y) * HOVER_SMOOTH_FACTOR * delta

func _on_mouse_entered():
	hovered = true

func _on_mouse_exited():
	hovered = false
